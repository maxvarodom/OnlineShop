import RxSwift

internal class FoodsService: BaseService<BaseRequestModel, FoodsResponseModel> {
	
	override func callService(request: BaseRequestModel = BaseRequestModel()) -> Observable<FoodsResponseModel> {
		return buildRequest(url: Router.foods(parameters: request.toJSON()), requestModel: request)
	}
}
