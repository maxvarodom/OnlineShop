import RxSwift
import RxCocoa
import Alamofire

class BaseService<Request: BaseRequestModel, Response: BaseResponseModel> {
    
	init() {}
	
	func validateRequest(request: Request) throws { }
	
	func validateResponse(response: Response) throws -> Response {
		guard response.responseHeader?.response == "success" else {
			throw ServiceError.response
		}
		guard response.responseHeader?.statusCode == "0000" else {
			throw ServiceError.statusCode
		}
		return response
	}
	
	func callService( request: Request) -> Observable<Response>  {
		preconditionFailure("Call service must be overridden")
	}
	
	func buildRequest(url: URLRequestConvertible, requestModel: Request) -> Observable<Response>  {
		return Maybe.just((url,requestModel))
			.subscribeOn(concurrentScheduler)
			.observeOn(concurrentScheduler)
			.map({ (urlRequestConvertible,request) -> (URLRequestConvertible) in
				try self.validateRequest(request: request)
				return (url)
			})
			.asObservable()
			.flatMap({ Manager.shareInstance.sessionURL(urlRequest: $0) })
			.map({ return try JsonMapper.shareInstance.mapper($0.result.value) })
			.map { try self.validateResponse(response: $0) }
			.catchError { VerifyErrorType.shareInstance.verifyErrorType($0) }
	}
}
