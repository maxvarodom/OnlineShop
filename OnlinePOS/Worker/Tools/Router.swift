import Alamofire

enum Router : URLRequestConvertible {
	
	case foods(parameters: Parameters)
	case customUrl(parameters: Parameters)
	
	var method: HTTPMethod {
		switch self {
		case .foods: return .put
		case .customUrl: return .post
		}
	}
	
	var path: String {
		switch self {
		case .foods: return "5b7137523200006a00f36c11"
		case .customUrl: return ""
		}
	}
	
	var parameters: Parameters {
		switch self {
		case .foods(let parameter),
			 .customUrl(let parameter):
			return parameter
		}
	}
}

extension Router {
	
	func asURLRequest() throws -> URLRequest {
		
		guard let url = try? URL(string: "http://www.mocky.io/v2/")?.asURL() else {
			throw WorkerError.notHost
		}
		
		guard let urlUnWraper = url else {
			throw WorkerError.notHost
		}
		
		var urlRequest = URLRequest(url: urlUnWraper.appendingPathComponent(path))
		urlRequest.httpMethod = method.rawValue
		urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
		let jsonData = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
		urlRequest.httpBody = jsonData
		return urlRequest
		
	}
}
