import Foundation
import RxSwift

public enum RepeatBehavior {
	case immediate (maxCount: UInt)
	case delayed (maxCount: UInt, time: Double)
	case exponentialDelayed (maxCount: UInt, initial: Double, multiplier: Double)
	case customTimerDelayed (maxCount: UInt, delayCalculator: (UInt) -> Double)
}

public typealias RetryPredicate = (Error) -> Bool

extension RepeatBehavior {

	func calculateConditions(_ currentRepetition: UInt) -> (maxCount: UInt, delay: Double) {
		switch self {
		case .immediate(let max):
			return (maxCount: max, delay: 0.0)
		case .delayed(let max, let time):
			return (maxCount: max, delay: time)
		case .exponentialDelayed(let max, let initial, let multiplier):
			let delay = currentRepetition == 1 ? initial : initial * pow(1 + multiplier, Double(currentRepetition - 1))
			return (maxCount: max, delay: delay)
		case .customTimerDelayed(let max, let delayCalculator):
			return (maxCount: max, delay: delayCalculator(currentRepetition))
		}
	}
}

extension ObservableType {

	public func retry(_ behavior: RepeatBehavior, scheduler: SchedulerType = MainScheduler.instance, shouldRetry: RetryPredicate? = nil) -> Observable<E> {
		return retry(1, behavior: behavior, scheduler: scheduler, shouldRetry: shouldRetry)
	}
	
	internal func retry(_ currentAttempt: UInt, behavior: RepeatBehavior, scheduler: SchedulerType = MainScheduler.instance, shouldRetry: RetryPredicate? = nil)
		-> Observable<E> {
			guard currentAttempt > 0 else { return Observable.empty() }
			
			let conditions = behavior.calculateConditions(currentAttempt)
			
			return catchError { error -> Observable<E> in
				guard conditions.maxCount > currentAttempt else { return Observable.error(error) }
				
				if let shouldRetry = shouldRetry, !shouldRetry(error) {
					return Observable.error(error)
				}
				
				guard conditions.delay > 0.0 else {
					return self.retry(currentAttempt + 1, behavior: behavior, scheduler: scheduler, shouldRetry: shouldRetry)
				}
				
				return Observable<Void>.just(()).delaySubscription(conditions.delay, scheduler: scheduler).flatMapLatest {
					self.retry(currentAttempt + 1, behavior: behavior, scheduler: scheduler, shouldRetry: shouldRetry)
				}
			}
	}
}
