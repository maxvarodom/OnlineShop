import RxSwift

class VerifyErrorType {

    static let shareInstance = VerifyErrorType()

    func verifyErrorType<Response>(_ error: Error) -> Observable<Response> {
        let errorMessage = "\(error)"
        if (errorMessage.range(of: "Code=-1009") != nil) {
            return Observable.error(WorkerError.noInternetConnection)
        } else if (errorMessage.range(of: "Code=-1001") != nil) {
            return Observable.error(WorkerError.noInternetConnection)
        } else if (errorMessage.range(of: "Code=-1003") != nil) {
            return Observable.error(WorkerError.hostNameCouldNotBeFound)
        } else if (errorMessage.range(of: "Code=3840") != nil) {
            return Observable.error(WorkerError.cannonMapperFailNotJSON)
        } else if (errorMessage.range(of: "Code=-1005") != nil) {
            return Observable.error(WorkerError.serverDown)
        } else {
            return Observable.error(error)
        }
    }
}
