enum WorkerError: Error {
    case mapperError
	case cannonMapperFailNotJSON
    case successFail
    case successNil
    case noInternetConnection
    case hostNameCouldNotBeFound
    case linkNotFound
    case serverDown
    case notHasKeyUrlFormRequestModel
	case notHost
}

enum ServiceError: Error {
	case statusCode
	case response
}

enum AppError: Error {
	case fullCredit
	case idCard
	case expiredDate
	case idCardsBehind
	case saveInHistoryToInternal
	case saveInProductToInternal
	case other
	case outOfStock
	case notEnough
}
