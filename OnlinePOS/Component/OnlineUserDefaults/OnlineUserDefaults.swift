import Foundation

class OnlineUserDefaults {
	
	static let `standard` = OnlineUserDefaults()
	
	private let keyProductsFirst = "T233-252WS-FS5E-POWS"
	
	enum DonwloadData: Int {
		case needNewInformation = 0
		case wasAlready = 1
	}
	
	func saveFlagProductsFirst(flag: DonwloadData) {
		UserDefaults.standard.set(flag.rawValue, forKey: keyProductsFirst)
	}
	
	func getFlagProductsFirst() -> OnlineUserDefaults.DonwloadData {
		return OnlineUserDefaults.DonwloadData.init(rawValue: UserDefaults.standard.integer(forKey: keyProductsFirst)) ?? .needNewInformation
	}
}
