struct Words {
	static let productName = "ชื่อสินค้า"
	static let productNumber = "จำนวนสินค้า"
	static let price = "ราคาสินค้า"
	static let shoppingBasket = "ตากร้าสินค้า"
	static let amountProduct = "จำนวนสินค้าทั้งหมด"
	static let priceTotalProduct = "ราคาสินค้าทั้งหมด"
	static let statusCard = "สถานะ: -"
}
