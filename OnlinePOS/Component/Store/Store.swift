import UIKit
import ObjectMapper

fileprivate let productDocumentDirectory = FileManager.init().urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
fileprivate let productArchiveURL = productDocumentDirectory.appendingPathComponent("productDocumentDirectory")
fileprivate let historyDocumentDirectory = FileManager.init().urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
fileprivate let historyArchiveURL = productDocumentDirectory.appendingPathComponent("historyDocumentDirectory")

final internal class Store {
	
	static let `default` = Store()
	static let `test` = Store()

	let cards: [CardModel] = [
		CardModel.init(idCard: "4242-4242-4242-4242", expiredDate: "11/11", idCardsBehind: "111"),
		CardModel.init(idCard: "1111-2222-3333-4444", expiredDate: "11/11", idCardsBehind: "111"),
		CardModel.init(idCard: "3333-3333-3333-3333", expiredDate: "33/33", idCardsBehind: "333"),
		CardModel.init(idCard: "1111-1111-1111-1111", expiredDate: "11/11", idCardsBehind: "111"),
		CardModel.init(idCard: "2222-2222-2222-2222", expiredDate: "11/11", idCardsBehind: "111")
	]
	
	@discardableResult
	func saveProduct(by products: FoodsResponseModel) -> Bool {
		return NSKeyedArchiver.archiveRootObject(products.toJSON(), toFile: productArchiveURL.path)
	}
	
	@discardableResult
	func getProduct() -> FoodsResponseModel {
		let directory = NSKeyedUnarchiver.unarchiveObject(withFile: productArchiveURL.path) as? [String: Any] ?? [:]
		let mapper = Mapper<FoodsResponseModel>().map(JSON: directory)
		return mapper ?? FoodsResponseModel()
	}
	
	@discardableResult
	func saveHistory(by mapHistoryModel: MapHistoryModel) -> Bool {
		let history = getHistory()
		if (history.mapHistoryModel ?? [] ).isEmpty {
			history.mapHistoryModel = []
			history.mapHistoryModel?.append(mapHistoryModel)
		} else {
			history.mapHistoryModel?.append(mapHistoryModel)
		}
		return NSKeyedArchiver.archiveRootObject(history.toJSON(), toFile: historyArchiveURL.path)
	}
	
	@discardableResult
	func getHistory() -> HistoryModel {
		let directory = NSKeyedUnarchiver.unarchiveObject(withFile: historyArchiveURL.path) as? [String: Any] ?? [:]
		let mapper = Mapper<HistoryModel>().map(JSON: directory)
		return mapper ?? HistoryModel()
	}
	
	@discardableResult
	func clearHistory() -> Bool {
		return NSKeyedArchiver.archiveRootObject([:], toFile: historyArchiveURL.path)
	}
	
	@discardableResult
	func clearProducts() -> Bool {
		return NSKeyedArchiver.archiveRootObject([:], toFile: productArchiveURL.path)
	}
}
