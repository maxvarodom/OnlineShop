import RxCocoa
import RxSwift

final internal class HomeViewModel: BaseViewModel {
	
	private var basket = [MapFoodsResponseModel]()
	private var productsVariableSubject = Variable<[MapFoodsResponseModel]>.init([])
		
	func fistDownloadProductsInto(flag: OnlineUserDefaults.DonwloadData) -> Observable<(OnlineUserDefaults.DonwloadData)> {
		if flag == .needNewInformation {
			return downloadProductFormExternal()
		} else {
			return downloadProdcutFormInternalStore()
		}
	}
	
	func getFlagProductsFirst() -> OnlineUserDefaults.DonwloadData {
		return OnlineUserDefaults.standard.getFlagProductsFirst()
	}
	
	func clearAllProducts() {
		if Flag.clearAllProducts {
			clearBasketValue()
			Flag.clearAllProducts = false
		}
	}
	
	func deductItemsAfterBuySuccessfully() {
		if Flag.deductSuccess {
			clearBasketValue()
			Flag.deductSuccess = false
		}
	}
	
	func clearBasketValue() {
		basket = []
	}
	
	func getProductsForPresentCollectionView() -> Observable<[MapFoodsResponseModel]> {
		return productsVariableSubject.asObservable()
	}
	
	func getBasket() -> [MapFoodsResponseModel] {
		return basket
	}
	
	func addProduct(reference product: MapFoodsResponseModel) -> Observable<Int> {
		return Observable<Int>.create({ (observer) -> Disposable in
			guard self.validateAmountProduct(product: product) else {
				self.handleErrorAlertViewsDelegate?.handleAlertViews(error: AppError.outOfStock)
				return Disposables.create()
			}
			self.basket.append(product)
			observer.onNext(self.getAmountInBasket())
			return Disposables.create()
		})
	}
	
	func validateAmountProduct(product: MapFoodsResponseModel) -> Bool {
		return product.count ?? 0 > 0
	}
	
	func getAmountInBasket() -> Int {
		return self.basket.count
	}
}

extension HomeViewModel {
	
	private func downloadProductFormExternal() -> Observable<(OnlineUserDefaults.DonwloadData)> {
		let eventDownloadProduct = FoodsService()
			.callService()
			.map({ (foodsResponseModel) -> [MapFoodsResponseModel] in
				guard Store.default.saveProduct(by: foodsResponseModel) else {
					return []
				}
				OnlineUserDefaults.standard.saveFlagProductsFirst(flag: .wasAlready)
				return foodsResponseModel.foodsResponseModel ?? []
			})
			.subscribeOn(MainScheduler.asyncInstance)
			.observeOn(MainScheduler.asyncInstance)
			.asDriver(onErrorRecover: { (error) -> SharedSequence<DriverSharingStrategy, [MapFoodsResponseModel]> in
				self.handleErrorAlertViewsDelegate?.handleAlertViews(error: error)
				let empty = SharedSequence<DriverSharingStrategy, [MapFoodsResponseModel]>.empty()
				return empty
			})
			.map { (mapFoodsResponseModel) -> OnlineUserDefaults.DonwloadData in
				self.productsVariableSubject.value = mapFoodsResponseModel
				return .needNewInformation
			}
			.asObservable()
		return eventDownloadProduct
	}
	
	private func downloadProdcutFormInternalStore() -> Observable<(OnlineUserDefaults.DonwloadData)> {
		self.productsVariableSubject.value = Store.default.getProduct().foodsResponseModel ?? []
		return Observable.just(.wasAlready)
	}
}
