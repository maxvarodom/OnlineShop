import UIKit
import RxSwift
import RxCocoa

final internal class HomeViewController: BaseViewController<HomeViewModel> {
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var shoppingBasketBarButtonItem: UIBarButtonItem!
	@IBOutlet weak var historyBarButtonItem: UIBarButtonItem!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupEventHistoryBarButtonItem().disposed(by: disposeBag)
		setupEventShoppingBasketBarButtonItem().disposed(by: disposeBag)
		registerCellToCollectionView()
		setupCollectionViewForPresentProduct().disposed(by: disposeBag)
		setupCollectionViewAction().disposed(by: disposeBag)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.clearAllProducts()
		viewModel.deductItemsAfterBuySuccessfully()
		setViewRightBarButtonItem(amountProduct: viewModel.getAmountInBasket())
		viewModel.fistDownloadProductsInto(flag: viewModel.getFlagProductsFirst()).subscribe().disposed(by: disposeBag)
	}
	
	private func setupEventHistoryBarButtonItem() -> Disposable {
		return historyBarButtonItem.rx.tap.bind { [weak self] _ in self?.router?.presentHistoryScreen() }
	}
	
	private func setupEventShoppingBasketBarButtonItem() -> Disposable {
		return shoppingBasketBarButtonItem.rx.tap
			.bind { [weak self] _ in
				self?.router?.presentBasketScreen(prepare: self?.viewModel.getBasket() ?? [])
		}
	}
	
	private func registerCellToCollectionView() {
		collectionView.register(type: ProductCollectionViewCell.self)
	}
	
	private func setupCollectionViewForPresentProduct() -> Disposable {
		return viewModel
			.getProductsForPresentCollectionView()
			.bind(to: collectionView.rx.items(cellType: ProductCollectionViewCell.self))
			{ row, mapFoodRepsonseModel, productCollectionViewCell in
				productCollectionViewCell.presentViews(by: mapFoodRepsonseModel)
		}
	}
	
	private func setupCollectionViewAction() -> Disposable {
		return collectionView.rx
			.modelSelected(MapFoodsResponseModel.self)
			.flatMap({ (mapFoodRepsonseModel) in
				return self.viewModel.addProduct(reference: mapFoodRepsonseModel)
			})
			.bind(onNext: { [weak self] (countProduct) in
				self?.setViewRightBarButtonItem(amountProduct: countProduct)
			})
	}
	
	private func setViewRightBarButtonItem(amountProduct: Int) {
		navigationItem.rightBarButtonItem?.title = "\(Words.shoppingBasket) (\(amountProduct))"
	}
}
