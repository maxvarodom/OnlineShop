import UIKit
import RxCocoa
import RxSwift

final internal class DetailHistoryViewController: BaseViewController<DetailHistoryViewModel> {
	
	@IBOutlet weak var tableView: UITableView!
	
	var prepareMapFoodsResponseModel: [MapFoodsResponseModel]?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		registerCellToTableView()
		setupPresentDetailHistoryToTableView().disposed(by: disposeBag)
	}
	
	func registerCellToTableView() {
		tableView.register(type: DetailProductTableViewCell.self)
	}
	
	func setupPresentDetailHistoryToTableView() -> Disposable {
		return Observable<[MapFoodsResponseModel]>.just(prepareMapFoodsResponseModel ?? [])
			.bind(to: tableView.rx.items(cellType: DetailProductTableViewCell.self))
			{ row, element, cell in
				cell.presentViews(by: element)
		}
	}
}
