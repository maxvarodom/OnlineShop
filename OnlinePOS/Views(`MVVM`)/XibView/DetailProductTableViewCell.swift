import UIKit

@IBDesignable final internal class DetailProductTableViewCell: UITableViewCell {
	@IBOutlet weak var urlImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	
	func presentViews(by product: MapFoodsResponseModel) {
		urlImageView.setImageURL(url: product.imageUrl ?? "")
		nameLabel.text = "\(Words.productName) \(product.name ?? "")"
		priceLabel.text = "\(Words.price) \(product.price?.description ?? "")"
	}
}
