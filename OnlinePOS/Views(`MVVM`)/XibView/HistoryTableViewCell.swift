import UIKit

@IBDesignable final internal class HistoryTableViewCell: UITableViewCell {
	@IBOutlet weak var dateLabel: UILabel!
}
