import UIKit

@IBDesignable final internal class ProductCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var countLabel: UILabel!
	
	func presentViews(by product: MapFoodsResponseModel) {
		imageView.setImageURL(url: product.imageUrl ?? "")
		nameLabel.text = "\(Words.productName) \(product.name ?? "")"
		priceLabel.text = "\(Words.price) \(product.price?.description ?? "")"
		countLabel.text = "\(Words.productNumber) \(product.count?.description ?? "")"
	}
}
