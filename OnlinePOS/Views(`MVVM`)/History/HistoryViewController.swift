import UIKit
import RxCocoa
import RxSwift

final internal class HistoryViewController: BaseViewController<HistoryViewModel> {
	
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		registerCellToTableView()
		setupPresentHistoryToTableView().disposed(by: disposeBag)
		setupEventActionPresentDetailHistoryFromTableView().disposed(by: disposeBag)
	}
	
	func registerCellToTableView()  {
		tableView.register(type: HistoryTableViewCell.self)
	}
	
	func setupPresentHistoryToTableView() -> Disposable {
		return viewModel
			.getHistoryPresentToTableView(history: viewModel.getHistoryFromInternal())
			.bind(to: tableView.rx.items(cellType: HistoryTableViewCell.self))
			{ row, element, cell in
				cell.dateLabel.text = element.date?.description
		}
	}
	
	func setupEventActionPresentDetailHistoryFromTableView() -> Disposable {
		return tableView.rx.modelSelected(MapHistoryModel.self)
			.bind(onNext: { (detailHistory) in
				self.router?.presentDetailHistoryScreen(prepare: detailHistory.mapFoodsResponseModel ?? [])
			})
	}
}
