import RxSwift
import RxCocoa

final internal class HistoryViewModel: BaseViewModel {
	
	func getHistoryPresentToTableView(history: HistoryModel) -> Observable<[MapHistoryModel]> {
		return Observable.just(history)
			.map { historyModel -> [MapHistoryModel] in
				return historyModel.mapHistoryModel ?? []
		}
	}
	
	func getHistoryFromInternal() -> HistoryModel {
		return Store.default.getHistory()
	}
}
