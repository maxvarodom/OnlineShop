import UIKit
import RxSwift

internal class BaseViewController<ViewModel>: UIViewController where ViewModel: BaseViewModel {
	
	var viewModel: ViewModel!
	var router: ViewControllerRouter?
	var disposeBag = DisposeBag()
	
	private var setTitle: String? {
		switch self {
		case is HomeViewController: return "หน้าหลัก"
		case is HistoryViewController: return "ประวัติ"
		case is BasketViewController: return "ตากร้าสินค้า"
		case is DetailHistoryViewController: return "รายละเอียดประวัติสินค้า"
		default: return nil
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = setTitle
		viewModel = ViewModel()
		viewModel.handleErrorAlertViewsDelegate = self
		router = ViewControllerRouter.init(viewController: self)
	}
}

extension BaseViewController: HandleErrorAlertViewsDelegate {
	
	func handleAlertViews(error: Error) {
		if let serviceError = error as? ServiceError {
			alertView(message: "\(serviceError)", preferredStyle: .alert)
		}
		
		if let workerError = error as? WorkerError {
			alertView(message: "\(workerError)", preferredStyle: .alert)
		}
		
		if let appError = error as? AppError {
			alertView(message: "\(appError)", preferredStyle: .alert)
		}
	}
	
	private func alertView(title: String? = nil, message: String? = nil, preferredStyle: UIAlertControllerStyle) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(alert, animated: true)
	}
}
