import RxSwift
import RxCocoa

internal protocol DataViewModelInitiable {
	init()
}
