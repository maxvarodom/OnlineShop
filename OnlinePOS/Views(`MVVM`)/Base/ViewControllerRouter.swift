import UIKit

class ViewControllerRouter {
	
	var viewController: UIViewController?
	
	init(viewController: UIViewController) {
		self.viewController = viewController
	}
	
	enum StoryboardName: String {
		case home = "Home"
		case history = "History"
		case basket = "Basket"
		case detailHistory = "DetailHistory"
	}
	
	enum ViewControllerWithIdentifierName: String {
		case homeIdentifier = "HomeViewController"
		case historyIdentifier = "HistoryViewController"
		case basketIdentifier = "BasketViewController"
		case detailHistoryIdentifier = "DetailHistoryViewController"
	}
}

extension ViewControllerRouter {
	
	func presentHistoryScreen() {
		let storyboard = UIStoryboard(name: StoryboardName.history.rawValue, bundle: nil)
		if let historyViewController = storyboard.instantiateViewController(withIdentifier: ViewControllerWithIdentifierName.historyIdentifier.rawValue) as? HistoryViewController {
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(historyViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(historyViewController, animated: true)
		}
	}
	
	func presentBasketScreen(prepare mapFoodsResponseModel: [MapFoodsResponseModel]) {
		let storyboard = UIStoryboard(name: StoryboardName.basket.rawValue, bundle: nil)
		if let basketViewController = storyboard.instantiateViewController(withIdentifier: ViewControllerWithIdentifierName.basketIdentifier.rawValue) as? BasketViewController {
			basketViewController.prepareMapFoodsResponseModel = mapFoodsResponseModel
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(basketViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(basketViewController, animated: true)
		}
	}
	
	func presentDetailHistoryScreen(prepare mapFoodsResponseModel: [MapFoodsResponseModel]) {
		let storyboard = UIStoryboard(name: StoryboardName.detailHistory.rawValue, bundle: nil)
		if let detailHistoryViewController = storyboard.instantiateViewController(withIdentifier: ViewControllerWithIdentifierName.detailHistoryIdentifier.rawValue) as? DetailHistoryViewController {
			detailHistoryViewController.prepareMapFoodsResponseModel = mapFoodsResponseModel
			guard let navigationController = viewController?.navigationController else {
				viewController?.present(detailHistoryViewController, animated: true, completion: nil)
				return
			}
			navigationController.pushViewController(detailHistoryViewController, animated: true)
		}
	}
}

