internal class BaseViewModel: DataViewModelInitiable {
	
	required init() { }
	
	weak var handleErrorAlertViewsDelegate: HandleErrorAlertViewsDelegate?
	
}
