import UIKit
import RxCocoa
import RxSwift

final internal class BasketViewController: BaseViewController<BasketViewModel> {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var inputCreditCardTextField: UITextField!
	@IBOutlet weak var inputExpiredDateTextField: UITextField!
	@IBOutlet weak var inputIDCardsBehindTextField: UITextField!
	@IBOutlet weak var statusCardLabel: UILabel!
	@IBOutlet weak var amountLabel: UILabel!
	@IBOutlet weak var priceTotalLabel: UILabel!
	@IBOutlet weak var clearAllProductsButton: UIButton!
	@IBOutlet weak var buyButton: UIButton!
	
	var prepareMapFoodsResponseModel: [MapFoodsResponseModel]?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		registerCellToTableView()
		setupPresentProductToTableView().disposed(by: disposeBag)
		setupPresentAmountAndTotalProducts()
		setupEventToTextField()
		setupEventClearAllProductsButton().disposed(by: disposeBag)
		setupEventToBuyButton().disposed(by: disposeBag)
	}
	
	func registerCellToTableView() {
		tableView.register(type: DetailProductTableViewCell.self)
	}
	
	func setupPresentProductToTableView() -> Disposable {
		return Observable.just(prepareMapFoodsResponseModel)
			.map { products in return products ?? [] }
			.bind(to: tableView.rx.items(cellType: DetailProductTableViewCell.self))
			{ row, element, cell in
				cell.presentViews(by: element)
		}
	}
	
	func setupEventToTextField() {
		let eventNumberCreditCard = inputCreditCardTextField.rx.text
		let eventExpiredDate = inputExpiredDateTextField.rx.text
		let eventIDCardsBehind = inputIDCardsBehindTextField.rx.text
		let eventTextField = Observable.combineLatest(eventNumberCreditCard, eventExpiredDate, eventIDCardsBehind)
			.skip(1)
			.map { [weak self] numberCard, expriedDate, idCardBehine -> BasketViewModel.ValidateCard in
				self?.inputCreditCardTextField.text = numberCard!.formatCreditCard()
				self?.inputExpiredDateTextField.text = expriedDate!.formatExpiredDate()
				self?.inputIDCardsBehindTextField.text = idCardBehine!.formatIDCardsBehind()
				let cardModel = CardModel.init(idCard: numberCard!, expiredDate: expriedDate!, idCardsBehind: idCardBehine!)
				return self?.viewModel.validateCard(by: cardModel,
													to: (numberCard: self?.inputCreditCardTextField,
														 expriedDate: self?.inputExpiredDateTextField,
														 idCardBehine: self?.inputIDCardsBehindTextField)) ?? .none
		}
		
		eventTextField
			.map({ (validateCard) -> String in
				return "\(Words.statusCard) \(validateCard)"
			})
			.bind(to: statusCardLabel.rx.text)
			.disposed(by: disposeBag)
		
		eventTextField
			.retry()
			.map({ (validateCard) -> UIColor in
				if validateCard == .incorrect { return .red }
				if validateCard == .correct { return .green }
				return .black
			})
			.bind(to: statusCardLabeTextColor)
			.disposed(by: disposeBag)
	}
	
	var statusCardLabeTextColor: Binder<UIColor> {
		return Binder(statusCardLabel.rx.base) { textField, color in
			textField.textColor = color
		}
	}
	
	func setupPresentAmountAndTotalProducts() {
		let calculateTotal = viewModel.calculateTotal(products: prepareMapFoodsResponseModel ?? [])
		amountLabel.text = "\(Words.amountProduct) \(calculateTotal.amount)"
		priceTotalLabel.text = "\(Words.priceTotalProduct) \(calculateTotal.priceTotal)"
	}
	
	func setupEventClearAllProductsButton() -> Disposable {
		return clearAllProductsButton.rx.tap
			.bind { [weak self] _ in
				self?.viewModel.clearAllProducts()
				self?.navigationController?.popViewController(animated: true)
		}
	}
	
	func setupEventToBuyButton() -> Disposable {
		return buyButton.rx.tap
			.bind { [weak self] _ in
				
				let cardModel = CardModel(idCard: self?.inputCreditCardTextField.text! ?? "",
										  expiredDate: self?.inputExpiredDateTextField.text! ?? "",
										  idCardsBehind: self?.inputIDCardsBehindTextField.text! ?? "")
				
				let mapHistotyModel = MapHistoryModel(date: Date().description,
													  cardModel: cardModel,
													  mapFoodsResponseModel: self?.prepareMapFoodsResponseModel ?? [])
				
				guard let buy = self?.viewModel.buy(card: cardModel, prepare: mapHistotyModel, prepareMapFoodsResponseModel: self?.prepareMapFoodsResponseModel ?? []) else {
					return
				}
				
				if buy.isSuccess {
					guard self?.viewModel.saveProductToInternal(products: buy.product) ?? false else {
						return
					}
					self?.navigationController?.popViewController(animated: true)
				}
		}
	}
}
