import UIKit
import RxSwift
import RxCocoa

final internal class BasketViewModel: BaseViewModel {
	
	let getCardFromStore = Store.default.cards
	
	enum ValidateCard {
		case correct
		case incorrect
		case none
	}
	
	func buy(card: CardModel, prepare mapHistoryModel: MapHistoryModel, prepareMapFoodsResponseModel: [MapFoodsResponseModel]) -> (product: FoodsResponseModel, isSuccess: Bool) {
		do {
			try validateCard(by: card)
			
			guard saveInHistory(by: mapHistoryModel) else {
				handleErrorAlertViewsDelegate?.handleAlertViews(error: AppError.saveInHistoryToInternal)
				return (FoodsResponseModel(), false)
			}
			
			let product = try deductOfProducts(from: getProductFromInternal(), products: prepareMapFoodsResponseModel)
			
			Flag.deductSuccess = true
			return (product, true)
		} catch let error {
			handleErrorAlertViewsDelegate?.handleAlertViews(error: error)
			return (FoodsResponseModel(), false)
		}
	}
	
	func clearAllProducts() {
		Flag.clearAllProducts = true
	}
	
	func validateCard(by card: CardModel) throws {
		
		let validateIdCard = getCardFromStore.contains(where: { (cardModel) -> Bool in
			return card.idCard == cardModel.idCard
		})
		
		let validateExpiredDate = getCardFromStore.contains(where: { (cardModel) -> Bool in
			return card.expiredDate == cardModel.expiredDate
		})
		
		let validateIdCardsBehind = getCardFromStore.contains(where: { (cardModel) -> Bool in
			return card.idCardsBehind == cardModel.idCardsBehind
		})
		
		let validateFullCredit = getCardFromStore.contains(where: { (cardModel) -> Bool in
			return !(card.idCard == "1111-1111-1111-1111" || card.idCard == "2222-2222-2222-2222")
		})
		
		guard validateIdCard else {
			throw AppError.idCard
		}
		
		guard validateExpiredDate else {
			throw AppError.expiredDate
		}
		
		guard validateIdCardsBehind else {
			throw AppError.idCardsBehind
		}
		
		guard validateFullCredit else {
			throw AppError.fullCredit
		}
		
		guard validateIdCard else {
			throw AppError.idCard
		}
	}
	
	func validateCard(by card: CardModel, to textFields: (numberCard: UITextField?, expriedDate: UITextField?, idCardBehine: UITextField?)) -> BasketViewModel.ValidateCard {
		let formatCardOutput = formatCard(by: card)
		textFields.numberCard?.text = formatCardOutput.numberCard
		textFields.expriedDate?.text = formatCardOutput.expriedDate
		textFields.idCardBehine?.text = formatCardOutput.idCardBehine
		
		let validateIdCard = getCardFromStore.contains(where: { (cardModel) -> Bool in
			return formatCardOutput.numberCard == cardModel.idCard
		})
		
		guard validateIdCard else {
			return .incorrect
		}
		
		return .correct
	}
	
	func formatCard(by card: CardModel) -> (numberCard: String?, expriedDate: String?, idCardBehine: String?) {
		return (card.idCard?.formatCreditCard(),
				card.expiredDate?.formatExpiredDate(),
				card.idCardsBehind?.formatIDCardsBehind())
	}
	
	func calculateTotal(products: [MapFoodsResponseModel]) -> (amount: Int, priceTotal: Int) {
		let amount = products.count
		var priceTotal = Int()
		products.forEach { (product) in
			priceTotal += (product.price ?? 0)
		}
		return (amount: amount, priceTotal: priceTotal)
	}
	
	@discardableResult
	func deductOfProducts(from product: FoodsResponseModel, products: [MapFoodsResponseModel]) throws -> FoodsResponseModel {
		let getProductFormInternal = product
		let lessProduct = try getProductFormInternal.foodsResponseModel?
			.map({ (product) -> (MapFoodsResponseModel) in
				try products.forEach({ (sourceProduct) in
					if product.id == sourceProduct.id {
						if product.count != nil {
							guard self.validateAmountProduct(product: product) else {
								throw AppError.notEnough
							}
							product.count! -= 1
						}
					}
				})
				return product
			})
		getProductFormInternal.foodsResponseModel = lessProduct
		return getProductFormInternal
	}
	
	func validateAmountProduct(product: MapFoodsResponseModel) -> Bool {
		return product.count ?? 0 > 0
	}
	
	func getProductFromInternal() -> FoodsResponseModel {
		return Store.default.getProduct()
	}
	
	func saveProductToInternal(products: FoodsResponseModel) -> Bool {
		let isSave = Store.default.saveProduct(by: products)
		if !isSave {
			handleErrorAlertViewsDelegate?.handleAlertViews(error: AppError.saveInProductToInternal)
		}
		return isSave
	}
	
	func saveInHistory(by mapHistoryModel: MapHistoryModel) -> Bool {
		return Store.default.saveHistory(by: mapHistoryModel)
	}
}
