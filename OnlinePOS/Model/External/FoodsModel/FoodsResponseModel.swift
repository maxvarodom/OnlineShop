import ObjectMapper

internal class FoodsResponseModel: BaseResponseModel {
	
	var foodsResponseModel: [MapFoodsResponseModel]?
	
	override init() {
		super.init()
	}
	
	required init?(map: Map) {
		super.init(map: map)
	}
	
	override func mapping(map: Map) {
		super.mapping(map: map)
		foodsResponseModel <- map["responseFoods"]
	}
}
