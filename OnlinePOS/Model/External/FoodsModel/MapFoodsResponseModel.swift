import ObjectMapper

internal class MapFoodsResponseModel: Mappable {
	
	var id: String?
	var name: String?
	var price: Int?
	var count: Int?
	var imageUrl: String?
	
	init() { }
	
	init(id: String, name: String, price: Int, count: Int, imageUrl: String) {
		self.id = id
		self.name = name
		self.price = price
		self.count = count
		self.imageUrl = imageUrl
	}
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
		price <- map["price"]
		count <- map["count"]
		imageUrl <- map["imageUrl"]
	}
}
