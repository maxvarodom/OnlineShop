import ObjectMapper

class BaseRequestModel: Mappable {
	
	init() { }
	
	required init?(map: Map) { }
	
	func mapping(map: Map) { }
	
}
