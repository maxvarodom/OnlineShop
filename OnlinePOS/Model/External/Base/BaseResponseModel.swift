import ObjectMapper

class BaseResponseModel: Mappable {
	
	var responseHeader: BaseResponseHeaderModel?
	
	init() { }
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		responseHeader <- map["responseHeader"]
	}
}
