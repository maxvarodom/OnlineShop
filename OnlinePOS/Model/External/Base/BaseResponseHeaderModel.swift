import ObjectMapper

internal class BaseResponseHeaderModel: Mappable {
	
	var response: String?
	var statusCode: String?
	
	init() { }
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		response <- map["response"]
		statusCode <- map["statusCode"]
	}
}
