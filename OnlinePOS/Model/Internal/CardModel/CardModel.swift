import ObjectMapper

class CardModel: Mappable {
	
	var idCard: String?
	var expiredDate: String?
	var idCardsBehind: String?
	
	init(idCard: String, expiredDate: String, idCardsBehind: String) {
		self.idCard = idCard
		self.expiredDate = expiredDate
		self.idCardsBehind = idCardsBehind
	}
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		idCard <- map["idCard"]
		expiredDate <- map["expiredDate"]
		idCardsBehind <- map["idCardsBehind"]
	}
}
