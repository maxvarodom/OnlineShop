import ObjectMapper

class MapHistoryModel: Mappable {
	
	var date: String?
	var cardModel: CardModel?
	var mapFoodsResponseModel: [MapFoodsResponseModel]?
	
	init() { }
	
	init(date: String, cardModel: CardModel, mapFoodsResponseModel: [MapFoodsResponseModel]) {
		self.date = date
		self.cardModel = cardModel
		self.mapFoodsResponseModel = mapFoodsResponseModel
	}
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		date <- map["date"]
		cardModel <- map["cardModel"]
		mapFoodsResponseModel <- map["mapFoodsResponseModel"]
	}
}
