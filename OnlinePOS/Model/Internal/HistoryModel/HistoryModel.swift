import ObjectMapper

class HistoryModel: Mappable {
	
	var mapHistoryModel: [MapHistoryModel]?
	
	init() { }
	
	init(mapHistoryModel: [MapHistoryModel]) {
		self.mapHistoryModel = mapHistoryModel
	}
	
	required init?(map: Map) { }
	
	func mapping(map: Map) {
		mapHistoryModel <- map["mapHistoryModel"]
	}
}
