import UIKit
import AlamofireImage

extension UIImageView {
	
	@discardableResult
	func setImageURL(url: String) -> Self {
		guard let urlOfURL = URL(string: url) else {
			return self
		}
		self.af_setImage(withURL: urlOfURL)
		return self
	}
}
