import UIKit
import RxSwift
import RxCocoa
import RxDataSources

extension Reactive where Base: UITableView {
	
	internal func items<S: Sequence, Cell: UITableViewCell, O : ObservableType>
		(cellType: Cell.Type = Cell.self)
		-> (_ source: O)
		-> (_ configureCell: @escaping (Int, S.Iterator.Element, Cell) -> Void)
		-> Disposable
		where O.E == S {
			let buildToString = String(describing: Cell.self)
			return self.items(cellIdentifier: buildToString, cellType: Cell.self)
	}
}
