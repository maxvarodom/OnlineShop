import UIKit

extension UICollectionView {
	
	final internal func register(type classType: Any) {
		let buildToString = String(describing: classType)
		register(UINib.init(nibName: buildToString, bundle: nil), forCellWithReuseIdentifier: buildToString)
	}
}
