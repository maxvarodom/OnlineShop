import UIKit
import AnyFormatKit

extension String {
	
	func formatCreditCard() -> String {
		let string = self.digits
		let format = TextInputFormatter(textPattern: "####-####-####-####", prefix: "+16")
		return format.formattedText(from: string) ?? ""
	}
	
	func formatExpiredDate() -> String {
		let string = self.digits
		let format = TextInputFormatter(textPattern: "##/##", prefix: "+4")
		return format.formattedText(from: string) ?? ""
	}
	
	func formatIDCardsBehind() -> String {
		let string = self.digits
		let format = TextInputFormatter(textPattern: "###", prefix: "+3")
		return format.formattedText(from: string) ?? ""
	}
	
	func removeFormat() -> String {
		return self.filter({ (character) -> Bool in
			return character != "-" && character != "/"
		})
	}
	
	var digits: String {
		return components(separatedBy: CharacterSet.decimalDigits.inverted)
			.joined()
	}
}
