extension String {
	
	func format() -> String {
		if range.location == 19 {
			return false
		}
		
		if range.length == 1 {
			if (range.location == 5 || range.location == 10 || range.location == 15) {
				let text = textField.text ?? ""
				textField.text = text.substring(to: text.index(before: text.endIndex))
			}
			return true
		}
		
		if (range.location == 4 || range.location == 9 || range.location == 14) {
			textField.text = String(format: "%@ ", textField.text ?? "")
		}
		
		return true
	}
}
