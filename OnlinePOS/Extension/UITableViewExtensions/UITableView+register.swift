import UIKit

extension UITableView {
	
	final internal func register(type classType: Any) {
		let buildToString = String(describing: classType)
		register(UINib.init(nibName: buildToString, bundle: nil), forCellReuseIdentifier: buildToString)
	}
}
