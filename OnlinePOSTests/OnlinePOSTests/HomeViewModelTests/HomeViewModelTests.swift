@testable import OnlinePOS
import XCTest
import RxBlocking
import RxTest

class HomeViewModelTests: XCTestCase {
	
	override func setUp() {
		super.setUp()
		Flag.clearAllProducts = false
		Flag.deductSuccess = false
		OnlineUserDefaults.standard.saveFlagProductsFirst(flag: .needNewInformation)
	}
	
	override func tearDown() {
		super.tearDown()
		Flag.clearAllProducts = false
		Flag.deductSuccess = false
		OnlineUserDefaults.standard.saveFlagProductsFirst(flag: .needNewInformation)
	}
	
	func testFirstDownLoadDataExternalSendFlag() {
		XCTAssertEqual(OnlineUserDefaults.standard.getFlagProductsFirst(), .needNewInformation)
		let homeViewModel = HomeViewModel()
		let observer = homeViewModel
			.fistDownloadProductsInto(flag: .needNewInformation)
			.toBlocking()
			.materialize()
		
		switch observer {
		case .completed(elements: let element):
			XCTAssertEqual(element.first, .needNewInformation)
		default:
			XCTFail()
		}
		XCTAssertEqual(OnlineUserDefaults.standard.getFlagProductsFirst(), .wasAlready)
	}
	
	func testFirstDownLoadDataInternalSendFlag() {
		let homeViewModel = HomeViewModel()
		let observer = homeViewModel
			.fistDownloadProductsInto(flag: .wasAlready)
			.toBlocking()
			.materialize()
		
		switch observer {
		case .completed(elements: let element):
			XCTAssertEqual(element.first, .wasAlready)
		default:
			XCTFail()
		}
	}
	
	func testClearAllProductsSendFlag() {
		let homeViewModel = HomeViewModel()
		homeViewModel.clearAllProducts()
		XCTAssertEqual(Flag.clearAllProducts, false)
	}
	
	func testDeductItemsAfterBuySuccessfully() {
		let homeViewModel = HomeViewModel()
		homeViewModel.deductItemsAfterBuySuccessfully()
		XCTAssertEqual(Flag.deductSuccess, false)
	}
	
	func testBasketClearValue() {
		let homeViewModel = HomeViewModel()
		homeViewModel.clearBasketValue()
		XCTAssertTrue(homeViewModel.getBasket().isEmpty)
	}
	
	func testAddProductCountNumberProduct() {
		let homeViewModel = HomeViewModel()
		let product = MapFoodsResponseModel.init(id: "1", name: "หมู", price: 1, count: 10, imageUrl: "")
		let observer1 = try! homeViewModel
			.addProduct(reference: product)
			.toBlocking()
			.first()
		
		XCTAssertEqual(observer1, 1)
		
		let observer2 = try! homeViewModel
			.addProduct(reference: product)
			.toBlocking()
			.first()
		
		XCTAssertEqual(observer2, 2)

		let observer3 = try! homeViewModel
			.addProduct(reference: product)
			.toBlocking()
			.first()
		
		XCTAssertEqual(observer3, 3)
	}
	
	func testValidateAmountProductSuccess() {
		let homeViewModel = HomeViewModel()
		let product = MapFoodsResponseModel.init(id: "", name: "", price: 1, count: 1, imageUrl: "")
		let validate = homeViewModel.validateAmountProduct(product: product)
		XCTAssertTrue(validate)
	}
	
	func testValidateAmountProductFail() {
		let homeViewModel = HomeViewModel()
		let product = MapFoodsResponseModel.init(id: "", name: "", price: 1, count: 0, imageUrl: "")
		let validate = homeViewModel.validateAmountProduct(product: product)
		XCTAssertFalse(validate)
	}
}
