@testable import OnlinePOS
import XCTest

class BasketViewModelTests: XCTestCase {
	
	override func setUp() {
		super.setUp()
		Flag.clearAllProducts = false
		Flag.deductSuccess = false
	}
	
	override func tearDown() {
		super.tearDown()
		Flag.clearAllProducts = false
		Flag.deductSuccess = false
	}
	
	func testBuyProductsNumberCardFailFormat() {
		let basketViewModel = BasketViewModel()
		let cardModel = CardModel.init(idCard: "1245-5225", expiredDate: "11/11", idCardsBehind: "123")
		let testValidateCard = basketViewModel.buy(card: cardModel, prepare: MapHistoryModel(), prepareMapFoodsResponseModel: [])
		XCTAssertFalse(testValidateCard.isSuccess)
	}
	
	func testBuyProductValidateCardExpriedDateNotStraightSendFalse() {
		let basketViewModel = BasketViewModel()
		let cardModel = CardModel.init(idCard: "4242-4242-4242-4242", expiredDate: "9/24", idCardsBehind: "111")
		let testValidateCard = basketViewModel.buy(card: cardModel, prepare: MapHistoryModel(), prepareMapFoodsResponseModel: [])
		XCTAssertFalse(testValidateCard.isSuccess)
	}

	func testBuyProductValidateIdCardBehindDateNotStraightSendFalse() {
		let basketViewModel = BasketViewModel()
		let cardModel = CardModel.init(idCard: "4242-4242-4242-4242", expiredDate: "11/11", idCardsBehind: "121")
		let testValidateCard = basketViewModel.buy(card: cardModel, prepare: MapHistoryModel(), prepareMapFoodsResponseModel: [])
		XCTAssertFalse(testValidateCard.isSuccess)
	}
	
	func testBuyProductValidateIdCardCorrect() {
		let basketViewModel = BasketViewModel()
		let cardModel = CardModel.init(idCard: "4242-4242-4242-4242", expiredDate: "11/11", idCardsBehind: "111")
		let testValidateCard = basketViewModel.buy(card: cardModel, prepare: MapHistoryModel(), prepareMapFoodsResponseModel: [])
		XCTAssertTrue(testValidateCard.isSuccess)
	}
	
	func testCheckFlagClearAllProducts() {
		let basketViewModel = BasketViewModel()
		basketViewModel.clearAllProducts()
		XCTAssertEqual(Flag.clearAllProducts, true)
	}
	
	func testThrowsValidateCardCompareCaseError() {
		let basketViewModel = BasketViewModel()
		let cardModelIdCardFail = CardModel.init(idCard: "4242-4242", expiredDate: "11/11", idCardsBehind: "111")
		let cardModelExpiredDateFail = CardModel.init(idCard: "4242-4242-4242-4242", expiredDate: "11/32", idCardsBehind: "111")
		let cardModelIdCardsBehindFail = CardModel.init(idCard: "4242-4242-4242-4242", expiredDate: "11/11", idCardsBehind: "121")
		let cardModelFullCredit = CardModel.init(idCard: "1111-1111-1111-1111", expiredDate: "11/11", idCardsBehind: "111")
		
		let expectation = self.expectation(description: "Test")
		
		do {
			try basketViewModel.validateCard(by: cardModelIdCardFail)
		} catch let appError {
			XCTAssertEqual(appError as! AppError, AppError.idCard)
		}
		
		do {
			try basketViewModel.validateCard(by: cardModelExpiredDateFail)
		} catch let appError {
			XCTAssertEqual(appError as! AppError, AppError.expiredDate)
		}

		do {
			try basketViewModel.validateCard(by: cardModelIdCardsBehindFail)
		} catch let appError {
			XCTAssertEqual(appError as! AppError, AppError.idCardsBehind)
		}

		do {
			try basketViewModel.validateCard(by: cardModelFullCredit)
		} catch let appError {
			XCTAssertEqual(appError as! AppError, AppError.fullCredit)
		}
		
		expectation.fulfill()
		waitForExpectations(timeout: 1.0, handler: nil)
	}
	
	func testValidateCardRealTimeCorrect() {
		let basketViewModel = BasketViewModel()
		let numberCardTextField = UITextField()
		let expriedDateTextField = UITextField()
		let idCardBehineTextField = UITextField()
		numberCardTextField.text = "4242-4242-4242-4242"
		expriedDateTextField.text = "11/11"
		idCardBehineTextField.text = "111"
		let cardModel = CardModel.init(idCard: numberCardTextField.text!, expiredDate: expriedDateTextField.text!, idCardsBehind: idCardBehineTextField.text!)
		let outputCaseCorrect = basketViewModel.validateCard(by: cardModel, to: (numberCardTextField, expriedDateTextField, idCardBehineTextField))
		XCTAssertEqual(outputCaseCorrect, .correct)
	}
	
	func testValidateCardRealTimeInCorrect() {
		let basketViewModel = BasketViewModel()
		let numberCardTextField = UITextField()
		let expriedDateTextField = UITextField()
		let idCardBehineTextField = UITextField()
		numberCardTextField.text = "4242-5525-5"
		expriedDateTextField.text = "11/11"
		idCardBehineTextField.text = "121"
		let cardModel = CardModel.init(idCard: numberCardTextField.text!, expiredDate: expriedDateTextField.text!, idCardsBehind: idCardBehineTextField.text!)
		let outputCaseInCorrect = basketViewModel.validateCard(by: cardModel, to: (numberCardTextField, expriedDateTextField, idCardBehineTextField))
		XCTAssertEqual(outputCaseInCorrect, .incorrect)
	}
	
	func testFormatCard() {
		let basketViewModel = BasketViewModel()
		let cardModel = CardModel.init(idCard: "1111111111111111", expiredDate: "1111", idCardsBehind: "111")
		let format = basketViewModel.formatCard(by: cardModel)
		XCTAssertEqual(format.numberCard, "1111-1111-1111-1111")
		XCTAssertEqual(format.expriedDate, "11/11")
		XCTAssertEqual(format.idCardBehine, "111")
	}
	
	func testProductsTotal() {
		let basketViewModel = BasketViewModel()
		let products = [
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
			MapFoodsResponseModel.init(id: "", name: "", price: 10, count: 1, imageUrl: ""),
		]
		let calculateTotal = basketViewModel.calculateTotal(products: products)
		XCTAssertEqual(calculateTotal.amount, 10)
		XCTAssertEqual(calculateTotal.priceTotal, 100)
	}
	
	func testDeductOfProducts()  {
		let basketViewModel = BasketViewModel()
		let products = FoodsResponseModel.init()
		
		products.foodsResponseModel = [
			MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "2", name: "B", price: 10, count: 10, imageUrl: "")
		]
		
		let buyProduct = [
			MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "2", name: "B", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "2", name: "B", price: 10, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "2", name: "B", price: 10, count: 10, imageUrl: "")
		]
		
		let calculate = try! basketViewModel.deductOfProducts(from: products, products: buyProduct)
		
		XCTAssertEqual(calculate.foodsResponseModel![0].count, 10 - 4)
		XCTAssertEqual(calculate.foodsResponseModel![1].count, 10 - 3)
	}
	
	func testValidateAmountProductLessThanZero() {
		let basketViewModel = BasketViewModel()
		let product = MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: -1, imageUrl: "")
		let validate = basketViewModel.validateAmountProduct(product: product)
		XCTAssertFalse(validate)
	}
	
	func testValidateAmountProductEqualToZero() {
		let basketViewModel = BasketViewModel()
		let product = MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 0, imageUrl: "")
		let validate = basketViewModel.validateAmountProduct(product: product)
		XCTAssertFalse(validate)
	}
	
	func testValidateAmountProductMoreThanZero() {
		let basketViewModel = BasketViewModel()
		let product = MapFoodsResponseModel.init(id: "1", name: "A", price: 10, count: 10, imageUrl: "")
		let validate = basketViewModel.validateAmountProduct(product: product)
		XCTAssertTrue(validate)
	}
}
