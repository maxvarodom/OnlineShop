@testable import OnlinePOS
import XCTest
import RxTest
import RxSwift
import RxBlocking

class HistoryViewModelTests: XCTestCase {
	
	override func setUp() {
		super.setUp()
		Flag.clearAllProducts = false
		Flag.deductSuccess = false
	}
	
	override func tearDown() {
		super.tearDown()
		Flag.clearAllProducts = false
		Flag.deductSuccess = false
	}
	
	func testGetHitory() {
		let historyViewModel = HistoryViewModel()
		let mapFoodResponseModel = [
			MapFoodsResponseModel.init(id: "1", name: "ไก่", price: 12, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "2", name: "หมู", price: 24, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "3", name: "หมา", price: 42, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "4", name: "ปลา", price: 32, count: 10, imageUrl: ""),
			MapFoodsResponseModel.init(id: "5", name: "นก", price: 14, count: 10, imageUrl: "")
		]
		let mapHistory = MapHistoryModel.init(date: Date().description, cardModel: CardModel.init(idCard: "", expiredDate: "", idCardsBehind: ""), mapFoodsResponseModel: mapFoodResponseModel)
		
		let careteHistory = HistoryModel.init()
		careteHistory.mapHistoryModel = [mapHistory]
		
		let observer = historyViewModel
			.getHistoryPresentToTableView(history: careteHistory)
			.toBlocking()
			.materialize()
		
		switch observer {
		case .completed(elements: let element):
			XCTAssertEqual(element.count, 1)
			XCTAssertEqual(element[0].first?.mapFoodsResponseModel?.count, 5)
		case .failed(elements: _, error: _):
			XCTFail()
		}
	}
}
