import Foundation

func readJSONFile(fileName: String) -> Any? {
	return Bundle.init(for: JSONFileReader.self)
		.path(forResource: fileName, ofType: "json")
		.flatMap { URL(fileURLWithPath: $0) }
		.flatMap { try? Data.init(contentsOf: $0) }
		.flatMap(JSONObjectWithData)
}

func JSONObjectWithData(formData data: Data) -> Any? {
	return try? JSONSerialization.jsonObject(with: data, options: [])
}

private class JSONFileReader {}
