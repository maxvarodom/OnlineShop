import XCTest
import ObjectMapper
import RxBlocking
import RxSwift
import RxTest
@testable import OnlinePOS

class VerifyErrorTypeTests: XCTestCase {
	
	enum BackendError: Error {
		case urlError(reason: String)
		case objectSerialization(reason: String)
		case objectDeletion(reason: String)
	}
	
	enum VerifyErrorTests: Error {
		case code1001(reason: String)
		case code1005(reason: String)
		case code1009(reason: String)
		case code1003(reason: String)
		case code3840(reason: String)
	}
	
	class SpyVerifyErrorType<Response>: VerifyErrorType {
		
		func spyVerifyErrorType(_ error: Error) -> Observable<Response> {
			return super.verifyErrorType(error)
		}
	}
	
	func testVerifyErrorTypeFromAlamofireRangString() {
		
		let listError: [VerifyErrorTests] = [
			VerifyErrorTests.code1001(reason: "Code=-1001"),
			VerifyErrorTests.code1003(reason: "Code=-1003"),
			VerifyErrorTests.code1005(reason: "Code=-1005"),
			VerifyErrorTests.code1009(reason: "Code=-1009"),
			VerifyErrorTests.code3840(reason: "Code=3840")
		]
		
		var count = Int()
		
		for error in listError {
			let verify = SpyVerifyErrorType<BaseResponseModel>()
				.spyVerifyErrorType(error)
				.toBlocking()
				.materialize()
			
			switch verify {
			case .completed(elements: _):
				XCTFail()
			case .failed(elements: _, error: let error):
				
				count += 1
				
				if count == 1 {
					XCTAssertEqual(error as! WorkerError, .noInternetConnection)
				}
				
				if count == 2 {
					XCTAssertEqual(error as! WorkerError, .hostNameCouldNotBeFound)
				}
				
				if count == 3 {
					XCTAssertEqual(error as! WorkerError, .serverDown)
				}
				
				if count == 4 {
					XCTAssertEqual(error as! WorkerError, .noInternetConnection)
				}
				
				if count == 5 {
					XCTAssertEqual(error as! WorkerError, .cannonMapperFailNotJSON)
				}
			}
		}
	}
}
