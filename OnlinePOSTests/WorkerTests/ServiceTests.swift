import XCTest
import Alamofire
import RxSwift
import RxBlocking
import RxTest
@testable import OnlinePOS

class ServiceTests: XCTestCase {
	
	func testServiceFoodsSuccess() {
		let callService = SpyFoodsService().callService(request: BaseRequestModel())
			.toBlocking()
		XCTAssertNotNil(try! callService.first())
	}
	
	func testServcieFoodsStatusCodeFail() {
		let spyFoodsService = SpyFoodsService()
		spyFoodsService.headerResponse = .fail
		let callService = spyFoodsService.callService(request: BaseRequestModel())
			.toBlocking()
		
		switch callService.materialize() {
		case .completed(elements: _):
			break
		case .failed(_, let error):
			let serviceError = error as! ServiceError
			XCTAssertTrue(serviceError == .response)
			return
		}
		
		XCTFail()
	}
	
	func testServcieFoodsHeaderResponseFail() {
		let spyFoodsService = SpyFoodsService()
		spyFoodsService.headerStatusCode = .statusCodeFail
		let callService = spyFoodsService.callService(request: BaseRequestModel())
			.toBlocking()
		
		switch callService.materialize() {
		case .completed(elements: _):
			break
		case .failed(_, let error):
			let serviceError = error as! ServiceError
			XCTAssertTrue(serviceError == .statusCode)
			return
		}
		
		XCTFail()
	}
	
	func testCallServiceToServer() {
		let expectation = self.expectation(description: "alamofire request expectation")
		_ = FoodsService()
			.callService()
			.subscribe(onNext: { (element) in
				debugPrint(element.toJSON())
				expectation.fulfill()
			}, onError: { error in
				debugPrint(error)
				XCTFail()
			})
		waitForExpectations(timeout: 10.0, handler: nil)
	}
}

class SpyFoodsService: FoodsService {
	
	enum HeaderStatusCode: String {
		case statusCodeOK = "0000"
		case statusCodeFail = "0001"
	}
	
	enum HeaderResponse: String {
		case success
		case fail
	}
	
	var headerStatusCode: HeaderStatusCode = .statusCodeOK
	var headerResponse: HeaderResponse = .success
	
	override func callService(request: BaseRequestModel) -> Observable<FoodsResponseModel> {
		return buildRequest(url: Router.foods(parameters: request.toJSON()), requestModel: request)
	}
	
	override func buildRequest(url: URLRequestConvertible, requestModel: BaseRequestModel) -> Observable<FoodsResponseModel> {
		let mock = FoodsResponseModel.init()
		let responseHeader = BaseResponseHeaderModel()
		responseHeader.response = headerResponse.rawValue
		responseHeader.statusCode = headerStatusCode.rawValue
		mock.responseHeader = responseHeader
		return Observable.just(mock)
			.map { element in try super.validateResponse(response: element) }
			.catchError({ (error) -> Observable<FoodsResponseModel> in
				return Observable.error(error)
			})
	}
}
