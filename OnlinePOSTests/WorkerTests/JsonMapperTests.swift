import XCTest
import ObjectMapper
@testable import OnlinePOS

class JsonMapperTests: XCTestCase {
	
	class JsonMapperTest<T>: JsonMapper where T: BaseMappable {
		
		func testMapper(response: Any?) -> T?  {
			return try! mapper(response) as T
		}
	}
	
	func testJsonMapper() {
		let jsonFile = readJSONFile(fileName: "MockJSON")
		let jsonTest = JsonMapperTest<FoodsResponseModel>().testMapper(response: jsonFile)
		XCTAssertNotNil(jsonTest)
	}
}
