import XCTest
@testable import OnlinePOS

class ReadFileJSONTests: XCTestCase {
	
	func testReadJSONFile() {
		let jsonFile = readJSONFile(fileName: "MockJSON")
		debugPrint(jsonFile as Any)
		XCTAssertNotNil(jsonFile)
	}
}
